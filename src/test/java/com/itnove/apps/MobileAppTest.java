package com.itnove.apps;

import com.itnove.utils.Utils;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by guillemhs on 2015-11-29.
 */
public class MobileAppTest {
    public RemoteWebDriver driver;
    public DesiredCapabilities caps = new DesiredCapabilities();
    public final String[] number1 = {"net.tecnotopia.SimpleCalculator:id/button1", "1"};
    public final String[] operAdd = {"net.tecnotopia.SimpleCalculator:id/buttonAdd", "+"};
    public final String[] number5 = {"net.tecnotopia.SimpleCalculator:id/button5", "5"};
    public final String[] equals = {"net.tecnotopia.SimpleCalculator:id/buttonEquals", "="};
    public final String[] result = {"net.tecnotopia.SimpleCalculator:id/display", "6"};

    public final List<By> locNumber1 = new ArrayList<>();
    public final List<By> locOperAdd = new ArrayList<>();
    public final List<By> locNumber5 = new ArrayList<>();
    public final List<By> locEquals = new ArrayList<>();
    public final List<By> locResult = new ArrayList<>();

    public String device;
    public int deviceNum;

    @Before
    public void setUp() throws MalformedURLException {
        // by default, start android app
        device = "ios";
        deviceNum = 0;
        // try to set according to user requirement
        if (System.getProperty("device") != null) {
            device = System.getProperty("device");
        } else {
            System.out.println("device not defined, will use default = Android");
        }
        deviceNum = device.equalsIgnoreCase("Android") ? 0 : 1;

        locNumber1.add(By.id(number1[0]));
        locNumber1.add(By.name(number1[1]));
        locOperAdd.add(By.id(operAdd[0]));
        locOperAdd.add(By.name(operAdd[1]));
        locNumber5.add(By.id(number5[0]));
        locNumber5.add(By.name(number5[1]));
        locEquals.add(By.id(equals[0]));
        locEquals.add(By.name(equals[1]));
        locResult.add(By.id(result[0]));
        locResult.add(By.name(result[1]));

        // switch between different browsers, e.g. iOS Safari or Android Chrome
        // let's use the os name to differentiate, because we only use default browser in that os
        if (device != null && device.equalsIgnoreCase("Android")) {
            File appDir = new File(Utils.getCurrentPath() + File.separator + "src" + File.separator + "test" + File.separator + "resources");
            File app = new File(appDir, "Calculator.apk");
            DesiredCapabilities caps = DesiredCapabilities.android();
            caps.setCapability("deviceName", "Android Emulator");
            caps.setCapability("app", app.getAbsolutePath());
            caps.setCapability("platformVersion", "5.1");
            driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), caps);
        } else {
            File appDir = new File(Utils.getCurrentPath() + File.separator + "src" + File.separator + "test" + File.separator + "resources");
            File app = new File(appDir, "Calculator.app");
            DesiredCapabilities caps = DesiredCapabilities.iphone();
            caps.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.6.3");
            caps.setCapability(MobileCapabilityType.BROWSER_NAME, "");
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
            caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 6s");
            caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9.3");
            caps.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
            caps.setCapability("xcodeOrgId", "3RYGCUBU3M");
            caps.setCapability("xcodeSigningId", "iPhone Developer");
            driver = new IOSDriver(new URL("http://0.0.0.0:4723/wd/hub"), caps);
        }
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
