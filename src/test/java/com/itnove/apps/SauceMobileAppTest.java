package com.itnove.apps;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by guillemhs on 2015-11-29.
 */
public class SauceMobileAppTest {
    public RemoteWebDriver driver;
    public final String[] number1 = {"net.tecnotopia.SimpleCalculator:id/button1", "1"};
    public final String[] operAdd = {"net.tecnotopia.SimpleCalculator:id/buttonAdd", "+"};
    public final String[] number5 = {"net.tecnotopia.SimpleCalculator:id/button5", "5"};
    public final String[] equals = {"net.tecnotopia.SimpleCalculator:id/buttonEquals", "="};
    public final String[] result = {"net.tecnotopia.SimpleCalculator:id/display", "6"};

    public final List<By> locNumber1 = new ArrayList<>();
    public final List<By> locOperAdd = new ArrayList<>();
    public final List<By> locNumber5 = new ArrayList<>();
    public final List<By> locEquals = new ArrayList<>();
    public final List<By> locResult = new ArrayList<>();

    public String device;
    public int deviceNum;

    @Before
    public void setUp() throws MalformedURLException {
        // by default, start android app
        device = "ios";
        deviceNum = 0;
        // try to set according to user requirement
        if (System.getProperty("device") != null) {
            device = System.getProperty("device");
        } else {
            System.out.println("device not defined, will use default = Android");
        }
        deviceNum = device.equalsIgnoreCase("Android") ? 0 : 1;

        locNumber1.add(By.id(number1[0]));
        locNumber1.add(By.name(number1[1]));
        locOperAdd.add(By.id(operAdd[0]));
        locOperAdd.add(By.name(operAdd[1]));
        locNumber5.add(By.id(number5[0]));
        locNumber5.add(By.name(number5[1]));
        locEquals.add(By.id(equals[0]));
        locEquals.add(By.name(equals[1]));
        locResult.add(By.id(result[0]));
        locResult.add(By.name(result[1]));

        // switch between diffrent browsers, e.g. iOS Safari or Android Chrome
        // let's use the os name to differentiate, because we only use default browser in that os
        if (device != null && device.equalsIgnoreCase("Android")) {
            DesiredCapabilities capabilities = DesiredCapabilities.android();
            capabilities.setCapability("appiumVersion", "1.4.16");
            capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
            capabilities.setCapability("deviceName", "Android Emulator");
            capabilities.setCapability("browserName", "");
            capabilities.setCapability("platformVersion", "4.4");
            capabilities.setCapability("platformName", "Android");
            capabilities.setCapability("app", "sauce-storage:Calculator.apk");
            driver = new AndroidDriver(new URL("https://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:443/wd/hub"), capabilities);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        } else {
            DesiredCapabilities caps = DesiredCapabilities.iphone();
            caps.setCapability("appiumVersion", "1.4.16");
            caps.setCapability("deviceName", "iPhone 6");
            caps.setCapability("deviceOrientation", "portrait");
            caps.setCapability("platformVersion", "9.2");
            caps.setCapability("platformName", "iOS");
            caps.setCapability("browserName", "");
            caps.setCapability("app", "sauce-storage:Calculator.zip");
            driver = new IOSDriver(new URL("https://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:443/wd/hub"), caps);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
