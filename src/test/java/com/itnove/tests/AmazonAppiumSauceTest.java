package com.itnove.tests;

import com.itnove.browsers.SauceBaseBrowserTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillemhs on 2015-11-16.
 */
public class AmazonAppiumSauceTest extends SauceBaseBrowserTest {

    /**
     * please run this test to make sure environment has been setup correctly
     */
    @Test
    public void searchTest() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,10);

        System.out.println(driver.getSessionId());

        driver.navigate().to("https://www.amazon.com");
        WebElement searchBox = driver.findElement(By.id("nav-search-keywords"));
        searchBox.sendKeys("tv 4k");

        WebElement searchButton = driver.findElement(By.xpath("//*[@id='nav-search-form']/div[1]/div/input"));
        searchButton.click();

        WebElement firstResult = driver.findElement(By.xpath("//*[@id='resultItems']/li[1]"));
        wait.until(ExpectedConditions.visibilityOf(firstResult));
        firstResult.click();

        WebElement itemTitle = driver.findElement(By.id("title"));
        wait.until(ExpectedConditions.visibilityOf(itemTitle));

        WebElement addToCart = driver.findElement(By.id("add-to-cart-button"));
        addToCart.click();

        WebElement viewCart = driver.findElement(By.xpath("//*[@id='nav-button-cart']/div/i"));
        viewCart.click();

        WebElement checkoutButton = driver.findElement(By.id("a-autoid-0-announce"));
        wait.until(ExpectedConditions.visibilityOf(checkoutButton));
        checkoutButton.click();

        WebElement signInSubmit = driver.findElement(By.id("signInSubmit"));
        wait.until(ExpectedConditions.visibilityOf(signInSubmit));

        Assert.assertTrue(signInSubmit.isDisplayed());
    }
}
