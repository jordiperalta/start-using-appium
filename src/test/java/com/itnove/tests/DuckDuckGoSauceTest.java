package com.itnove.tests;

import com.itnove.browsers.SauceBaseBrowserTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillemhs on 2015-11-16.
 */
public class DuckDuckGoSauceTest extends SauceBaseBrowserTest {

    /**
     * please run this test to make sure environment has been setup correctly
     */
    @Test
    public void searchTest() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,10);

        System.out.println(driver.getSessionId());

        driver.get("https://duckduckgo.com");
        WebElement searchBox = driver.findElement(By.id("search_form_input_homepage"));
        searchBox.click();
        searchBox.sendKeys("hawaiian pizza");
        WebElement searchButton = driver.findElement(By.id("search_button_homepage"));
        searchButton.click();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("id('links_wrapper')/div[1]"))));
        WebElement myResult = driver.findElement(By.id("r1-2"));
        myResult.click();
        Assert.assertFalse(driver.getCurrentUrl().contains("duckduckgo"));
    }
}
